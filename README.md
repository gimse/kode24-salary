# Tech Salary per Municipality in Norway

![](/screenshoot-webpage.png)

Estimating the salary for a developer in each municipality in Norway and showing it in a map. The analysis is based on the [Kode24 2023 Salary Survey](https://www.kode24.no/artikkel/dykk-ned-i-kode24s-lonnstall-vis-oss-hva-du-lager/79548382) 

## Getting Started

- Install Anaconda

- `conda create --name kode24-salary -y`

- `conda activate kode24-salary`

- `conda install --file requirements.txt -y`

- `jupyter notebook main.ipynb`